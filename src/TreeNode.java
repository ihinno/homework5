package src;

import java.util.*;

public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	public TreeNode(String n, TreeNode d, TreeNode r) {
		this.name = n;
		this.firstChild = d;
		this.nextSibling = r;
	}

	public TreeNode(String n) {
		this.name = n;
		this.firstChild = null;
		this.nextSibling = null;
		// TODO!!! Your constructor here
	}

	public static TreeNode parsePrefix(String s) {
		if (s == null || s == "")
			throw new RuntimeException("T�hi string sisendis " + s);
		if (s.contains(" ") || s.contains("\t"))
			throw new RuntimeException("Sisendis lubamatu t�hik " + s);
		LinkedList<TreeNode> mag = new LinkedList<TreeNode>();
		int tase = 1;
		StringTokenizer tokenizer = new StringTokenizer(s, "(,)", true);
		String token = tokenizer.nextToken();
		TreeNode uus;
		TreeNode juur = new TreeNode(token);
		mag.add(juur);
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			// System.out.println("m�rk = " + token);
			if (token.equals("(")) {
				if (mag.isEmpty())
					throw new RuntimeException("Eba�ige stringi esitus - puudub haru nimetus " + s);
				else
					token = tokenizer.nextToken();
				if (token.equals("(") || token.equals(",") || token.equals(")"))
					throw new RuntimeException("Eba�ige stringi esitus " + s);
				else
					// System.out.println("token lapseks = " + token);
					uus = new TreeNode(token);
				tase = tase + 1;
				mag.getLast().firstChild = uus;
				mag.add(uus);
			} else if (token.equals(")")) {
				if (mag.size() < 2)
					throw new RuntimeException("Eba�ige stringi esitus" + s);
				tase = tase - 1;
				if (mag.get(mag.size() - 2).nextSibling == null)
					mag.removeLast();
				else
					while (mag.size() > tase)
						mag.removeLast();

			} else if (token.equals(",")) {
				if (mag.size() < 2)
					throw new RuntimeException("Eba�ige stringi esitus " + s);
				else
					token = tokenizer.nextToken();
				// System.out.println("token s�saraks= " + token);
				if (token.equals("(") || token.equals(",") || token.equals(")"))
					throw new RuntimeException("Eba�ige stringi esitus " + s);
				else
					uus = new TreeNode(token);
				mag.getLast().nextSibling = uus;
				mag.add(uus);
			}

		}
		return juur; // TODO!!! return the root

	}

	public String rightParentheticRepresentation() {
		StringBuilder b = new StringBuilder();
		TreeNode current = this.firstChild;
		if (current != null)
			b.append("(");
		while (current != null) {
			b.append(current.rightParentheticRepresentation());
			current = current.nextSibling;
			if (current != null)
				b.append(",");
		}
		if (this.firstChild != null)
			b.append(")");
		b.append(name);
		return b.toString();

	}

	public static void main(String[] param) {
		String s1 = "A(B1,C,D)";
		TreeNode t1 = TreeNode.parsePrefix(s1);
		String r = t1.rightParentheticRepresentation();
		System.out.println("Tree: " + s1 + " ==> " + r);
		String s3 = "+(*(-(512,1),4),/(-6,3))";
		TreeNode t3 = TreeNode.parsePrefix(s3);
		String v3 = t3.rightParentheticRepresentation();
		System.out.println("Tree: " + s3 + " ==> " + v3); // A(B1,C,D) ==>
															// (B1,C,D)A
		String s5 = "6(5(1,3(2),4))";
		TreeNode t5 = TreeNode.parsePrefix(s5);
		String v5 = t5.rightParentheticRepresentation();
		System.out.println("Tree: " + s5 + " ==> " + v5); // A(B1,C,D) ==>
															// (B1,C,D)A
		String s6 = "+(*(-(2,1),4),/(6,3))";
		TreeNode t6 = TreeNode.parsePrefix(s6);
		String v6 = t6.rightParentheticRepresentation();
		System.out.println("Tree: " + s6 + " ==> " + v6); // A(B1,C,D) ==>
															// (B1,C,D)A

	}

}
